#!/bin/bash
metric_docker="docker-metric"
ts=`date +%s`
host=`hostname`
docker ps
if [[ $? -eq 0 ]]
then
    a=1
else
    a=2
fi
curl -X POST -d "[{\"metric\": \"$metric_docker\", \"endpoint\": \"$host\", \"timestamp\": $ts,\"step\": 60,\"value\": $a,\"counterType\": \"GAUGE\",\"tags\": \"idc=lg,project=xx\"}]" http://127.0.0.1:1988/v1/push
