#!/bin/bash
metric_node="node-status"
metric_pod="pod-status"
ts=`date +%s`
host=`hostname`
tim=`date '+%Y-%m-%d %H:%M:%S'`
nod=`/root/oc get node|grep "NotReady"|wc -l`
pod=`oc get pods --all-namespaces |grep "ContainerCreating\|Pending\|Terminating"  |awk '{ if  (((substr($6,length($6),length($6))) != "s" && (substr($6,length($6),length($6))) != "m")||(substr($6,1,(length($6)-1))>5 && (substr($6,length($6),length($6))) == "m" )) print $0}' |wc -l`

function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}

if [ "$nod" -ne "0" ]; then
	echo " some node is NotReady"
    a=1
    send $metric_node  $host $ts $a
else
    echo "Node status is ok"
    a=0
    send $metric_node  $host $ts $a
fi
if [ "$pod" -ne "0" ]; then
    echo " some node is NotReady"
    a=1
	echo $tim"`oc get pods --all-namespaces |grep "ContainerCreating\|Pending\|Terminating"  |awk '{ if  (((substr($6,length($6),length($6))) != "s" && (substr($6,length($6),length($6))) != "m")||(substr($6,1,(length($6)-1))>5 && (substr($6,length($6),length($6))) == "m" )) print $0}' `" >>/var/log/podstat.log
    send $metric_pod  $host $ts $a
else
    a=0
    send $metric_pod  $host $ts $a
fi
