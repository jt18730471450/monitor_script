#!/bin/bash
metric_lbr="sdn_lbr0"
metric_tun="sdn_tun0"

metric_rout1="route-172"
metric_rout2="route-10"

metric_ping="ping-internet"

ts=`date +%s`
host=`hostname`

lbr=`ip a|grep lbr0|wc -l`
tun=`ip a|grep tun0|wc -l`

rout1=`route|grep '172.30.0.0      0.0.0.0         255.255.0.0     U     0      0        0 tun0' |wc -l`
rout2=`route|grep '10.1.0.0        0.0.0.0         255.255.0.0     U     0      0        0 tun0' |wc -l`
ping1=`ping -c 3 www.jd.com | grep -q 'ttl=' && echo  "0"|| echo "1"`
ping2=`ping -c 3  github.com | grep -q 'ttl=' && echo  "0"|| echo "1"`


function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}

if [ "$lbr" -ne "0" ]; then
    a=0
    send $metric_lbr  $host $ts $a
else
    a=1
    send $metric_lbr  $host $ts $a
fi

if [ "$tun" -ne "2" ]; then
    a=1
    send $metric_tun  $host $ts $a
else
    a=0
    send $metric_tun  $host $ts $a
fi

if [ "$rout1" -ne "1" ]; then
    a=1
    send $metric_rout1  $host $ts $a
else
    a=0
    send $metric_rout1  $host $ts $a
fi

if [ "$rout2" -ne "1" ]; then
    a=1
    send $metric_rout2  $host $ts $a
else
    a=0
    send $metric_rout2  $host $ts $a
fi
if [ "$ping1" -ne "0" ] && [ "$ping2" -ne "0" ]; then
    a=1
    send $metric_ping  $host $ts $a
else
    a=0
    send $metric_ping  $host $ts $a
fi
