#!/bin/bash
metric_docker="NFS-DF"
ts=`date +%s`
host=`hostname`
df -h >/dev/null 2>&1
if [[ $? -eq 0 ]]
then
    a=0
else
    a=1
    df -h |grep 'Transport endpoint is not connected'|awk -F"‘" '{a=index($0,"‘");b=index($0,"’");print "fusermount-glusterfs -uz "substr($0,a+1,b-a-1)}'|sh
fi
curl -X POST -d "[{\"metric\": \"$metric_docker\", \"endpoint\": \"$host\", \"timestamp\": $ts,\"step\": 60,\"value\": $a,\"counterType\": \"GAUGE\",\"tags\": \"idc=lg,project=xx\"}]" http://127.0.0.1:1988/v1/push
