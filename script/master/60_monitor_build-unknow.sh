#!/bin/bash
list=()
ts=`date +%s`
python build.py |awk '{  split($2,ta,"-") ; print $1 " "  ta[length(ta)] " " $3" "$4" "$5}'|awk '{if(a[$1]<$3){a[$1]=$2;b[$1]=$3;c[$1]=$4;d[$1]=$5}}END{for(n in a)print n,b[n],a[n],c[n],d[n]}'|grep Fail|awk '{ if (systime()-$4<1800) print $0}'>tmp.1
while read line
do
	a=`echo $line|awk '{print $1 }'|awk -F":" '{print $1}'`
	b=`echo $line|awk '{print $1 }'|awk -F":" '{print $2}'`
	c=`echo $line|awk '{print $3 }'`
	d=` curl -s -k --cert /etc/origin/master/openshift-master.crt --key /etc/origin/master/openshift-master.key https://dev.dataos.io:8443/api/v1/namespaces/$a/pods/$b-$c-build/log -s |grep 'manifest unknown'|wc -l`
	if [[ $d -ne 0 ]]
	then
    	echo "`date` Namespace:$a Build-name:$b-$c-build" >>/var/log/build-unknowfest.log
	fi
	list[${#list[@]}]=$d
done <tmp.1
rm tmp.1
if [[ "${list[@]}" =~ 1 ]]
then
	a=1
else
	a=0
fi
curl -X POST -d "[{\"metric\": \"oo-monitor-build-unknow-mainfest\", \"endpoint\": \"openshift-container-deploy2.jcloud.local\", \"timestamp\": $ts,\"step\": 60,\"value\": $a,\"counterType\": \"GAUGE\",\"tags\": \"idc=lg,project=xx\"}]" http://127.0.0.1:1988/v1/push
