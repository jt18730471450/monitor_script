#!/bin/bash
metric_lvs="lvs-metric"
ts=`date +%s`
host=`hostname`

lv=`lvs |grep docker-pool |awk '{print $5}'`

if [ $(echo "$lv > 80"|bc) -eq 1 ]
then
    docker rmi $(docker images -q -f dangling=true)
	docker volume rm $(docker volume ls -q -f dangling=true)
else
    echo "a>b"
fi

function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}
send $metric_lvs  $host $ts $lv
