#!/bin/bash
metric_ping="pod-ping-status"
metric_svc="pod-curl-svc"
ts=`date +%s`
tim=`date '+%Y-%m-%d %H:%M:%S'`

function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}

function curl_svc {
	a=`oc  -n openfalcon-docker rsh  $1  curl https://172.30.0.1 -k|grep -q healthz && echo  "0"|| echo "1"`
}

function curl_baidu {
	b=` oc  -n openfalcon-docker rsh  $1  curl www.baidu.com -o /dev/null -s  -w %{http_code}`
}
for i in `oc get pod -n openfalcon-docker |grep agent|awk '{print $1}'`
do
	node=`oc  -n openfalcon-docker rsh $i cat /etc/hostname`
	curl_svc $i
	curl_baidu $i
	echo " $metric_ping $node $ts $a "
	echo " $metric_svc $node $ts $b"
done

