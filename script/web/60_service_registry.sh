#!/bin/bash
#adm_addr=("dtkl.app.asiainfodata.com" "dtklcd.app.asiainfodata.com" "dtklcdt.app.asiainfodata.com" "dtklcs.app.asiainfodata.com" "dtkldg.app.asiainfodata.com"  "dtklgz.app.asiainfodata.com" "dtklgz2.app.asiainfodata.com" "dtklxn.app.asiainfodata.com" "dtklyd.app.asiainfodata.com" "dtklzh.app.asiainfodata.com" "zhlygx.app.asiainfodata.com" "zhy.app.asiainfodata.com" "zhy2.app.asiainfodata.com")
metric="service-registry-datafoundry.cn-test"
host=`hostname`
ts=`date +%s`

function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}

token=`cat /tmp/token`
res=`curl -k  -L --connect-timeout 3 -w %{http_code} -o /dev/null -s registry.app.datafoundry.cn`
if [ $res -eq "000" ]; then
	res=0
fi
send $metric  $host $ts $res
echo $res
