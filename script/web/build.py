#!-*- coding:utf8 -*-
import time
import json
import datetime
import os
r = json.loads(os.popen("curl -k --cert /etc/origin/master/openshift-master.crt --key /etc/origin/master/openshift-master.key https://dev.dataos.io:8443/oapi/v1/builds  -s").read())
#print len(r)
for i in range(0,len(r["items"])):
    d = datetime.datetime.strptime(r["items"][i]["metadata"]["creationTimestamp"],"%Y-%m-%dT%H:%M:%SZ")
    f = str(int(time.mktime(d.timetuple())))
    print r["items"][i]["metadata"]["namespace"] + ":" + r["items"][i]["metadata"]["labels"]["buildconfig"] + " " + r["items"][i]["metadata"]["name"] + " " + r["items"][i]["status"]["phase"] + " " + f + " " +  r["items"][i]["metadata"]["creationTimestamp"]
