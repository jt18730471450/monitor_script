#!/bin/bash
#adm_addr=("dtkl.app.asiainfodata.com" "dtklcd.app.asiainfodata.com" "dtklcdt.app.asiainfodata.com" "dtklcs.app.asiainfodata.com" "dtkldg.app.asiainfodata.com"  "dtklgz.app.asiainfodata.com" "dtklgz2.app.asiainfodata.com" "dtklxn.app.asiainfodata.com" "dtklyd.app.asiainfodata.com" "dtklzh.app.asiainfodata.com" "zhlygx.app.asiainfodata.com" "zhy.app.asiainfodata.com" "zhy2.app.asiainfodata.com")
metric="service:"
adm_name=("gz_jiyisuo_data" "gz_jiyisuo_portal" "gz_jiyisuo_product" "wh_jiyisuo_data" "wh_jiyisuo_portal" "wh_jiyisuo_product")
adm_addr=("http://www.gzbdex.com/selects" "http://www.gzbdex.com" "http://www.gzbdex.com/gzselects" "www.cjbigdata.com/selects" "www.cjbigdata.com" "www.cjbigdata.com/whselects")
adm_res=("200" "200" "200" "200" "200" "200")

host=`hostname`
ts=`date +%s`

function send {
    curl -X POST -d "[{\"metric\": \"$1 $2\", \"endpoint\": \"$3\", \"timestamp\": $4,\"step\": 60,\"value\": $5,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}


for ((i=0;i<${#adm_addr[*]};i++));
do
	res=`curl -k  -L --connect-timeout 3 -w %{http_code} -o /dev/null -s ${adm_addr[$i]} `
	if [ $res -eq "000" ]; then
        res=0
    fi
	send $metric ${adm_name[$i]} $host $ts $res
	echo $res 
done
