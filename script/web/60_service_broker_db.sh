#!/bin/bash
svc_addr=("mysqlshare.servicebroker.dataos.io" "postgresqlshare.servicebroker.dataos.io" "greenplumshare.servicebroker.dataos.io" "dashboard.servicebroker.dataos.io")
svc_port=("3306" "5432" "5432" "27017")

adm_addr=("phpmyadmin-service-broker-db.app.dataos.io" "phppgadmin-service-broker-db.app.dataos.io" "phpgreenplumadmin-service-broker-db.app.dataos.io" "rockmongo-service-broker-db.app.dataos.io" "servicebroker-go-service-broker-db.app.dataos.io")
adm_res=("401" "200" "200" "200" "401")

host=`hostname`
ts=`date +%s`

function send {
    curl -X POST -d "[{\"metric\": \"service-broker_db:$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"servicebrokerdb\"}]" http://127.0.0.1:1988/v1/push
}

for ((i=0;i<${#svc_addr[*]};i++));
do
	tcping -t 3  ${svc_addr[$i]}  ${svc_port[$i]} 
	echo $?
#	send  ${svc_addr[$i]} $host $ts $? 
done

for ((i=0;i<${#adm_addr[*]};i++));
do
	res=`curl -k  -L --connect-timeout 3 -w %{http_code} -o /dev/null -s ${adm_addr[$i]} `
	if [ "$res" -ne "${adm_res[$i]}" ]; then
		echo "Connection to ${adm_addr[$i]} failed"
		a=1
		send ${adm_addr[$i]} $host $ts $a
	else
  		echo "Connection to ${adm_addr[$i]} succeeded"
		a=0
		send ${adm_addr[$i]} $host $ts $a 
	fi 
done
