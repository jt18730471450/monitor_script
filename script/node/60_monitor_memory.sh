#!/bin/bash
metric_docker="system-memory-free"
ts=`date +%s`
host=`hostname`
aa=`cat /proc/meminfo |grep -E "Cached|Buffers|MemFre|MemTotal"|grep -v Swap|awk 'BEGIN {a[0]=0;}{if (NR == 1){ a[0]=$2 } else {a[1]=a[1]+$2}}END{print a[1]/a[0]*100}'`
curl -X POST -d "[{\"metric\": \"$metric_docker\", \"endpoint\": \"$host\", \"timestamp\": $ts,\"step\": 60,\"value\": $aa,\"counterType\": \"GAUGE\",\"tags\": \"idc=lg,project=xx\"}]" http://127.0.0.1:1988/v1/push
