#!/bin/bash
metric_iptables="service-iptables"

ts=`date +%s`
host=`hostname`
ipta=`/bin/systemctl status  iptables.service |grep inactive|awk '{print $2}'|wc -l`


function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}

if [ "$ipta" -ne "0" ]; then
    a=1
    send $metric_iptables  $host $ts $a
else
    a=0
    send $metric_iptables  $host $ts $a
fi
