#!/bin/bash
metric_lvs="base-net-netconn"
ts=`date +%s`
host=`hostname`

lv=`netstat -antp|wc -l`

function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}
send $metric_lvs  $host $ts $lv
