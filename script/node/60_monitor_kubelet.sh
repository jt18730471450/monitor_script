#!/bin/bash
metric_node="kubelet-status"
ts=`date +%s`
host=`hostname`
tim=`date '+%Y-%m-%d %H:%M:%S'`
nod=`date |awk '{split($4,ta,":"); print "grep -E \""$2" "$3" "ta[1]":"ta[2]-1"|" $2 " "$3" " ta[1]":"ta[2]-2"|" $2 " "$3" " ta[1]":"ta[2]-3"|" $2 " "$3" " ta[1]":"ta[2]-4"|" $2 " "$3" " ta[1]":"ta[2]-5"\" /var/log/messages"}'|sh|grep kubelet.go|grep SyncLoop|wc -l`

function send {
    curl -X POST -d "[{\"metric\": \"$1\", \"endpoint\": \"$2\", \"timestamp\": $3,\"step\": 60,\"value\": $4,\"counterType\": \"GAUGE\",\"tags\": \"open\"}]" http://127.0.0.1:1988/v1/push
}

if [ "$nod" -ne "0" ]; then
    a=0
    send $metric_node  $host $ts $a
else
    a=1
    send $metric_node  $host $ts $a
fi
